<?php

namespace App\Http\Livewire;

use App\Models\Province;
use App\Models\Country;
use Livewire\Component;

class CountryProvinceSelect extends Component
{
    public $countries;
    public $provinces;

    public $name;
    public $country;
    public $province;

    public function mount()
    {
        $this->countries = Country::all();
        $this->provinces = collect();
    }

    public function updatedCountry($value)
    {
        $this->provinces = Province::where('country_id', $value)->get();
//        $this->province = $this->provinces->first()->id ?? null;
    }
}
