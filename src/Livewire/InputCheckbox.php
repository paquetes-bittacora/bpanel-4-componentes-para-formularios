<?php

namespace App\Http\Livewire;

use Livewire\Component;

class InputCheckbox extends Component
{
    public $name;
    public $idField;
    public $value = null;
    public $checked = false;
    public $multiple = false;
    public $title = null;
    public $labelText;
    public $disabled = null;

    public function render()
    {
        return view('livewire.input-checkbox')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'value' => $this->value,
            'checked' => $this->checked,
            'multiple' => $this->multiple,
            'labelText' => $this->labelText,
            'disabled' => $this->disabled,
        ]);
    }
}
