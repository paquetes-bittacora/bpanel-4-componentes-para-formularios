<?php

namespace App\Http\Livewire;

use App\Models\City;
use App\Models\Province;
use App\Models\Country;
use Livewire\Component;

class CountryProvinceCitySelect extends Component
{
    public $countries;
    public $provinces;
    public $cities;

    public $country_id;
    public $province_id;
    public $city_id;

    public function mount()
    {
        $this->countries = Country::all();
        if (!empty($this->country_id)){
            $this->provinces = Province::where('country_id', $this->country_id)->get();
        }
        else{
            $this->provinces = collect();
        }

        if (!empty($this->province_id)){
            $this->cities = City::where('province_id', $this->province_id)->get();
        }
        else{
            $this->cities = collect();
        }
    }

    public function changeCountry($value)
    {
        $this->province_id = '';
        $this->city_id = '';
        $this->provinces = Province::where('country_id', $value)->get();
        $this->cities = [];
    }

    public function changeProvince($value)
    {
        $this->city_id = '';
        $this->cities = City::where('province_id', $value)->get();
    }
}
