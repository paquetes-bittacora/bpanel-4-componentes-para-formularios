<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;

class DualListBox extends Component
{
    public string $name;
    public string $idField;
    /**
     * @var array Array de pares clave-valor. La clave es el ID del modelo, mientras que el value será el valor que se mostrará en pantalla.
     *
     * Ejemplo con usuarios: 'key' => $user->id, 'value' => $user->name
     */

    public array $allValues = [];

    /**
     * @var array Array de un nivel en el que solo estarán los valores que deben compararse con el array englobado en $allValues.
     *
     * Ejemplo de usuarios: [1,2,4,5,6]
     *
     * Se buscaría el valor de la key de $allValues dentro de este array.
     */
    public array $selectedValues = [];
    public ?string $labelText = null;
    public string $height = '232px';

    public function render()
    {
        return view('form-components::bpanel.livewire.dual-list-box')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'allValues' => $this->allValues,
            'selectedValues' => $this->selectedValues,
            'labelText' => $this->labelText,
            'height' => $this->height
        ]);
    }
}
