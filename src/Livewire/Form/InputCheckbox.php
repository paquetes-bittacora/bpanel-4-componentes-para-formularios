<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;

class InputCheckbox extends Component
{
    public $name;
    public $idField;
    public $value = null;
    public $checked = false;
    public $multiple = false;
    public $title = null;
    public $labelText;
    public $labelWidth = 3;
    public $fieldWidth = 9;
    public $bpanelForm = false;

    public function render()
    {
        return view('form-components::bpanel.livewire.input-checkbox')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'value' => $this->checked,
            'checked' => $this->checked,
            'multiple' => $this->multiple,
            'labelText' => $this->labelText,
            'labelWidth' => $this->labelWidth,
            'bpanelForm' => $this->bpanelForm,
            'fieldWidth' => $this->fieldWidth
        ]);
    }
}
