<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;

class InputText extends Component
{
    /**
     * Nombre del dato. Se utiliza para la propiedad "for" de la etiqueta, y para las propiedades name, id y aria-describedby del input.
     * @var
     */
    public $name;
    /**
     * Identificador del campo (OPCIONAL)
     * @var null
     */
    public $idField = null;
    /**
     * Utilizado para el placeholder (OPCIONAL)
     * @var null
     */
    public $placeholder = null;
    /**
     * Valor que tendrá el campo por defecto (OPCIONAL)
     * @var null
     */
    public $value = null;
    /**
     * Longitud mínima obligatoria del dato (OPCIONAL)
     * @var null
     */
    public $minlength = null;
    /**
     * Campo requerido (NO SOPORTADO POR SAFARI), escribe un * rojo, para indicar que hay campos obligatorios. (OPCIONAL)
     * @var bool
     */
    public $required = false;
    /**
     * Texto que tendrá la etiqueta
     * @var
     */
    public $labelText;
    /**
     * Número de columnas que ocupará la etiqueta (OPCIONAL)
     * @var null
     */
    public $labelWidth = 3;
    /**
     * Número de columnas que ocupará el campo (OPCIONAL)
     * @var null
     */
    public $fieldWidth = 7;
    /**
     * Expresión regular para la validación del valor introducido en el campo. (NO SOPORTADO POR SAFARI) (OPCIONAL)
     * @var null
     */
    public $pattern = null;
    /**
     * Atributo para marcar campo de solo lectura (OPCIONAL)
     * @var bool
     */
    public $readonly = false;
    /**
     * Número de caracteres de ancho que tendrá el campo (OPCIONAL)
     * @var null
     */
    public $size = null;
    /**
     * Indica si el valor del campo puede ser completado automáticamente por el navegador
     * (ver posibles valores en https://developer.mozilla.org/es/docs/Web/HTML/Elemento/input) (OPCIONAL)
     * @var null
     */
    public $autocomplete = null;
    /**
     * Indica si el campo aparecerá con el foco al cargar el documento (solo se permite un campo con un atributo de este tipo por documento) (OPCIONAL)
     * @var bool
     */
    public $autofocus = false;
    /**
     * Indica si el campo estará deshabilitado (OPCIONAL)
     * @var false
     */
    public $disabled = false;
    /**
     * Valor máximo (númerico o fecha-hora) para este elemento. No puede ser menor que su valor mínimo. (NO SOPORTADO POR IE Y SAFARI) (OPCIONAL)
     * @var null
     */
    public $max = null;
    /**
     * valor mínimo (numérico o fecha-hora) para este elemento. No puede ser menor que su valor mínimo. (NO SOPORTADO POR IE Y SAFARI) (OPCIONAL)
     * @var null
     */
    public $min = null;
    /**
     * Si se establece este atributo con valor TRUE, se está indicando que se debe revisar la ortografía y gramática del elemento.
     * El valor DEFAULT indica que el elemento va a actuar acorde al comportamiento predeterminado del navegador, posiblemente basado en el valor del atributo spellcheck de su elemento padre.
     * El valor FALSE indica que el elemento no debe ser revisado.
     * (OPCIONAL)
     * @var null
     */
    public $spellcheck = null;

    /**
     * Clases CSS Extras (Debe introducirse como un array)
     * @var null
     */
    public $cssClasses = null;

    /**
     * Sirve para poner un icono antes o después del campo de texto
     * (acepta un elemento <i> con una clase de fontawesome, un caracter de texto, etc.)
     * @var
     */
    public $icon = null;

    /**
     * Sirve para colocar a la izquierda o a la derecha del campo texto.
     * Valores: prepend (aparece a la izquierda) o append (aparece a la derecha)
     * @var null
     */
    public $alignIcon = null;

    public $language = null;

    public $image = null;

    public $customErrorMessage = null;

    public function render()
    {
        return view('form-components::bpanel.livewire.input-text')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'placeholder' => $this->placeholder,
            'value' => $this->value,
            'minlength' => $this->minlength,
            'required' => $this->required,
            'labelText' => $this->labelText,
            'labelWidth' => $this->labelWidth,
            'fieldWidth' => $this->fieldWidth,
            'pattern' => $this->pattern,
            'readonly' => $this->readonly,
            'size' => $this->size,
            'autocomplete' => $this->autocomplete,
            'autofocus' => $this->autofocus,
            'disabled' => $this->disabled,
            'max' => $this->max,
            'min' => $this->min,
            'spellcheck' => $this->spellcheck,
            'cssClasses' => $this->cssClasses,
            'icon' => $this->icon,
            'alignIcon' => $this->alignIcon,
            'language' => $this->language,
            'image' => $this->image,
            'customErrorMessage' => $this->customErrorMessage
        ]);
    }
}
