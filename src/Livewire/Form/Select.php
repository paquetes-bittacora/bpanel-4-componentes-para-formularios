<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;

class Select extends Component
{
    /**
     * Valor para el atributo 'name'
     * @var
     */
    public $name;
    /**
     * Valor para el atributo 'id'
     * @var null
     */
    public $idField = null;
    /**
     * Indica si el select es solo de una opción o múltiple (false si solo es para un valor, true para multiples)
     * @var bool
     */
    public $multiple = false;
    /**
     * @var
     */
    public $placeholder;
    /**
     * Propiedad que contendrá todos los valores del select
     * @var
     */
    public $allValues;
    /**
     * Array con valores simples que contendrá los valores que deben aparecer seleccionados
     * @var array
     */
    public $selectedValues = [];
    /**
     * Texto que tendrá la etiqueta
     * @var
     */
    public $labelText;
    /**
     * Número de columnas que ocupará la etiqueta (OPCIONAL)
     * @var null
     */
    public $labelWidth = 3;
    /**
     * Número de columnas que ocupará el campo (OPCIONAL)
     * @var null
     */
    public $fieldWidth = 6;
    /**
     * Indica si el campo estará deshabilitado (OPCIONAL)
     * @var false
     */
    public $disabled = false;
    /**
     * Campo requerido (NO SOPORTADO POR SAFARI), escribe un * rojo, para indicar que hay campos obligatorios. (OPCIONAL)
     * @var bool
     */
    public $required = false;
    /**
     * Campo de solo lectura.
     * @var bool
     */
    public $readonly = false;

    public function render()
    {
        return view('form-components::bpanel.livewire.select')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'multiple' => $this->multiple,
            'placeholder' => $this->placeholder,
            'allValues' => $this->allValues,
            'selectedValues' => $this->selectedValues,
            'labelText' => $this->labelText,
            'labelWidth' => $this->labelWidth,
            'fieldWidth' => $this->fieldWidth,
            'disabled' => $this->disabled,
            'required' => $this->required,
            'readonly' => $this->readonly
        ]);
    }
}
