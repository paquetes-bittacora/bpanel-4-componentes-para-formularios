<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;

class InputFile extends Component
{

    public string $name;
    public ?string $idField = null;
    public ?string $accept = null;
    public bool $capture = false;
    public ?array $files =  null;
    public bool $multiple = false;
    public bool $hideInput = false;
    public array $arrayFiles = [];
    public int $labelWidth = 3;
    public ?string $labelText = null;
    public int $fieldWidth = 7;
    public bool $required = false;
    public bool $showUpload = false;
    public bool $showCaption = true;
    public bool $showRemove = true;
    public int $minFileCount = 0;
    public int $maxFileCount = 0;
    public bool $dropZoneEnabled = true;
    public string $theme = 'fa';
    public string $language = 'es';
    public $allowedFileExtensions = null;
    public ?string $uploadUrl = null;
    public bool $browseOnZoneClick = true;
    public bool $csrf = false;
    public string $msgUploadError = 'Error en la subida del archivo';
    public bool $enableResumableUpload = false;
    public bool $uploadAsync = false;
    public bool $initialPreviewAsData = false;
    public bool $overwriteInitial = true;
    public ?string $deleteUrl = null;

    public function mount(){
        $this->allowedFileExtensions = explode(',', $this->allowedFileExtensions);
    }

    public function render()
    {
        return view('form-components::bpanel.livewire.input-file')->with([
            'accept' => $this->accept,
            'capture' => $this->capture,
            'files' => $this->files,
            'multiple' => $this->multiple,
            'labelWidth' => $this->labelWidth,
            'labelText' => $this->labelText,
            'fieldWidth' => $this->fieldWidth,
            'required' => $this->required,
            'showUpload' => $this->showUpload,
            'showCaption' => $this->showCaption,
            'showRemove' => $this->showRemove,
            'minFileCount' => $this->minFileCount,
            'maxFileCount' => $this->maxFileCount,
            'dropZoneEnabled' => $this->dropZoneEnabled,
            'theme' => $this->theme,
            'language' => $this->language,
            'hideInput' => $this->hideInput,
            'arrayFiles' => $this->arrayFiles,
            'allowedFileExtensions' => $this->allowedFileExtensions,
            'uploadUrl' => $this->uploadUrl,
            'browseOnZoneClick' => $this->browseOnZoneClick,
            'csrf' => $this->csrf,
            'msgUploadError' => $this->msgUploadError,
            'enableResumableUpload' => $this->enableResumableUpload,
            'uploadAsync' => $this->uploadAsync,
            'initialPreviewAsData' => $this->initialPreviewAsData,
            'overwriteInitial' => $this->overwriteInitial,
            'deleteUrl' => $this->deleteUrl
        ]);
    }
}
