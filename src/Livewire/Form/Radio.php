<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;

class Radio extends Component
{
    /**
     * Valor para el atributo 'name'
     * @var
     */
    public $name;
    /**
     * Valor para el atributo 'id'
     * @var null
     */
    public $idField = null;

    /**
     * Propiedad que contendrá las opciones disponibles
     * @var
     */
    public $options;

    /**
     * Propiedad que contendrá el valor de la opción seleccionada inicialmente
     * @var
     */
    public $selectedOption;

    /**
     * Texto que tendrá la etiqueta
     * @var
     */
    public $labelText;

    /**
     * Número de columnas que ocupará la etiqueta (OPCIONAL)
     * @var null
     */
    public $labelWidth = 3;

    /**
     * Número de columnas que ocupará el campo (OPCIONAL)
     * @var null
     */
    public $fieldWidth = 6;

    /**
     * Indica si el campo estará deshabilitado (OPCIONAL)
     * @var false
     */
    public $disabled = false;

    /**
     * Campo requerido (NO SOPORTADO POR SAFARI), escribe un * rojo, para indicar que hay campos obligatorios. (OPCIONAL)
     * @var bool
     */
    public $required = false;

    /**
     * Campo de solo lectura.
     * @var bool
     */
    public $readonly = false;

    public function render()
    {
        return view('form-components::bpanel.livewire.radio')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'options' => $this->options,
            'selectedOption' => $this->selectedOption,
            'labelText' => $this->labelText,
            'labelWidth' => $this->labelWidth,
            'fieldWidth' => $this->fieldWidth,
            'disabled' => $this->disabled,
            'required' => $this->required,
            'readonly' => $this->readonly
        ]);
    }
}
