<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;

class Textarea extends Component
{
    public $name;
    public $idField = null;
    public $required = false;
    public $disabled = false;
    public $labelWidth = 3;
    public $labelText = null;
    public $fieldWidth = 7;
    public $placeholder = null;
    public $icon = null;
    public $alignIcon = null;
    public $image = null;
    public $autocomplete = null;
    public $rows = 4;
    public $autofocus = null;
    public $cssClasses = [];
    public $value = null;
    public $readonly = null;
    public $customErrorMessage = null;

    public function render()
    {
        return view('form-components::bpanel.livewire.textarea')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'required' => $this->required,
            'disabled' => $this->disabled,
            'labelWidth' => $this->labelWidth,
            'labelText' => $this->labelText,
            'fieldWidth' => $this->fieldWidth,
            'placeholder' => $this->placeholder,
            'icon' => $this->icon,
            'alignIcon' => $this->alignIcon,
            'image' => $this->image,
            'autocomplete' => $this->autocomplete,
            'rows' => $this->rows,
            'autofocus' => $this->autofocus,
            'cssClasses' => $this->cssClasses,
            'value' => $this->value,
            'readonly' => $this->readonly,
            'customErrorMessage' => $this->customErrorMessage
        ]);
    }
}
