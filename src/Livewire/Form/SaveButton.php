<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;

class SaveButton extends Component
{
    public $arrTheme = [
        'save' => [
            'name' => 'save',
            'idField' => 'save',
            'text' => 'Guardar',
            'icon' => 'fa fa-save',
            'color' => 'success',
            'type' => 'submit'
        ],
        'reset' => [
            'name' => 'reset',
            'idField' => 'reset',
            'text' => 'Reset',
            'icon' => 'fa fa-undo',
            'color' => 'outline-lightgrey',
            'type' => 'reset'
        ],
        'update' => [
            'name' => 'update',
            'idField' => 'update',
            'text' => 'Actualizar',
            'icon' => 'fa fa-save',
            'color' => 'warning',
            'type' => 'update'
        ]
    ];

    /**
     * Valor para el atributo 'name'
     * @var
     */
    public $name;
    /**
     * Valor para el atributo 'id'
     * @var null
     */
    public $idField = null;
    /**
     * Texto del botón
     * @var
     */
    public $text;

    /**
     * Icono del botón (debe ser el contenido del atributo class completo. Ej: 'fa fa-facebook')
     * @var
     */
    public $icon;

    /**
     * Color de fondo del botón (default, primary, success, info, warning, danger)
     * Dirección del apartado de botones de Inspinia (http://webapplayers.com/inspinia_admin-v2.8/buttons.html)
     * @var
     */
    public $color;

    /**
     * submit, reset...
     * @var
     */
    public $type;

    /**
     * Si se mete un theme, se obtiene la información de $this->arrTheme
     * @var
     */
    public $theme;

    public function render()
    {
        $arr = [];
        if (isset($this->theme) and !empty($this->theme) and array_key_exists($this->theme, $this->arrTheme)) {
            $arr = $this->arrTheme[$this->theme];
            $this->text = $arr['text'];
            $this->icon = $arr['icon'];
            $this->type = $arr['type'];
            $this->name = $arr['name'];
            $this->idField = $arr['idField'];
            $this->color = $arr['color'];

        } else {
            $arr = [
                'text' => $this->text,
                'icon' => $this->icon,
                'type' => $this->type,
                'name' => $this->name,
                'idField' => $this->idField,
                'color' => $this->color,
            ];
        }

        return view('form-components::bpanel.livewire.save-button')->with($arr);
    }
}
