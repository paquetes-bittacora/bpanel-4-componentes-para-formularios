<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;

class Actions extends Component
{
    public $arrTheme = [
        'show' => [
            'name' => 'show',
            'idField' => 'show',
            'text' => 'Ver',
            'icon' => 'fa fa-find',
            'color' => 'blue',
            'type' => 'show',
            'permission' => 'view'
        ],
        'edit' => [
            'name' => 'edit',
            'idField' => 'edit',
            'text' => 'Editar',
            'icon' => 'fa fa-pencil',
            'color' => 'success',
            'type' => 'edit',
            'permission' => 'edit'
        ],
        'destroy' => [
            'name' => 'destroy',
            'idField' => 'destroy',
            'text' => 'Borrar',
            'icon' => 'fa fa-trash',
            'color' => 'danger',
            'type' => 'delete',
            'permission' => 'edit'
        ]
    ];

    /**
     * Valor para el atributo 'name'
     * @var
     */
    public $name;
    /**
     * Valor para el atributo 'id'
     * @var null
     */
    public $idField = null;
    /**
     * Texto del botón
     * @var
     */
    public $text;

    /**
     * Icono del botón (debe ser el contenido del atributo class completo. Ej: 'fa fa-facebook')
     * @var
     */
    public $icon;

    /**
     * Color de fondo del botón (default, primary, success, info, warning, danger)
     * Dirección del apartado de botones de Inspinia (http://webapplayers.com/inspinia_admin-v2.8/buttons.html)
     * @var
     */
    public $color;

    /**
     * submit, reset...
     * @var
     */
    public $type;

    /**
     * Si se mete un theme, se obtiene la información de $this->arrTheme
     * @var
     */
    public $theme;
    public $id;

    public function render()
    {
        $arr = [];
        if (isset($this->theme) and !empty($this->theme) and array_key_exists($this->theme, $this->arrTheme)) {
            $arr = $this->arrTheme[$this->theme];
            $this->text = $arr['text'];
            $this->icon = $arr['icon'];
            $this->type = $arr['type'];
            $this->name = $arr['name'];
            $this->idField = $arr['idField'];
            $this->color = $arr['color'];

        } else {
            $arr = [
                'text' => $this->text,
                'icon' => $this->icon,
                'type' => $this->type,
                'name' => $this->name,
                'idField' => $this->idField,
                'color' => $this->color,
            ];
        }

        return view('form-components::bpanel.livewire.actions')->with($arr)->with($id);
    }
}
