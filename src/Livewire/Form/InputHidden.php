<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;

class InputHidden extends Component
{
    public $name;
    public $idField = null;
    public $value = null;

    public function render()
    {
        return view('form-components::bpanel.livewire.input-hidden')->with([
            'name' => $this->name,
            'idField' => $this->idField,
            'value' => $this->value
        ]);
    }
}
