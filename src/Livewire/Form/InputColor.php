<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Livewire\Component;
use function view;

class InputColor extends Component
{
    public $name;
    public $idField = null;
    public $value = null;
    public $required = false;
    public $labelText;
    public $labelWidth = 3;
    public $fieldWidth = 7;
    public $disabled = false;
    public $customErrorMessage = null;

    public function render()
    {
        return view('form-components::bpanel.livewire.input-color');
    }
}
