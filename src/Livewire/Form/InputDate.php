<?php

namespace Bittacora\FormComponents\Livewire\Form;

use Carbon\Carbon;
use Livewire\Component;

class InputDate extends Component
{
    /**
     * Nombre del dato. Se utiliza para la propiedad "for" de la etiqueta, y para las propiedades name, id y aria-describedby del input.
     * @var
     */
    public $name;
    /**
     * Identificador del campo (OPCIONAL)
     * @var null
     */
    public $idField = null;
    /**
     * Campo requerido (NO SOPORTADO POR SAFARI), escribe un * rojo, para indicar que hay campos obligatorios. (OPCIONAL)
     * @var bool
     */
    public $required = false;
    /**
     * Texto que tendrá la etiqueta
     * @var
     */
    public $labelText;

    /**
     * Indica si el campo estará deshabilitado (OPCIONAL)
     * @var false
     */
    public $disabled = false;

    public $fieldWidth = 9;
    public $labelWidth = 3;
    public $defaultDate = false;
    public $enableTime = false;
    public $dateFormat = 'd/m/Y H:i';
    public $value = null;

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('form-components::bpanel.livewire.input-date');
    }

    public function mount(){
        if($this->defaultDate == false){
            $this->defaultDate = null;
        }else{
            $this->defaultDate = Carbon::now()->format('d-m-Y H:i');
        }
    }
}
