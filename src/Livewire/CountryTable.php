<?php

namespace App\Http\Livewire;

use App\Models\Country;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class CountryTable
{

    public $model = Country::class;

    public function builder()
    {

    }

    public function columns()
    {
        return [
            Column::name('name')->filterable()->searchable(),
            Column::callback(['id', 'name'], function ($id, $name) {
                return view('table-actions', ['id' => $id, 'name' => $name]);
            })
        ];
    }
}
