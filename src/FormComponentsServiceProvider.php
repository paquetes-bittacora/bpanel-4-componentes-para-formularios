<?php

namespace Bittacora\FormComponents;

use Bittacora\FormComponents\Livewire\Form\Actions;
use Bittacora\FormComponents\Livewire\Form\DualListBox;
use Bittacora\FormComponents\Livewire\Form\InputCheckbox;
use Bittacora\FormComponents\Livewire\Form\InputColor;
use Bittacora\FormComponents\Livewire\Form\InputDate;
use Bittacora\FormComponents\Livewire\Form\InputEmail;
use Bittacora\FormComponents\Livewire\Form\InputFile;
use Bittacora\FormComponents\Livewire\Form\InputHidden;
use Bittacora\FormComponents\Livewire\Form\InputNumber;
use Bittacora\FormComponents\Livewire\Form\InputPassword;
use Bittacora\FormComponents\Livewire\Form\InputText;
use Bittacora\FormComponents\Livewire\Form\InputTextLanguage;
use Bittacora\FormComponents\Livewire\Form\Radio;
use Bittacora\FormComponents\Livewire\Form\SaveButton;
use Bittacora\FormComponents\Livewire\Form\Select;
use Bittacora\FormComponents\Livewire\Form\Textarea;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\Blog\Commands\InstallCommand;

class FormComponentsServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('form-components')
            ->hasConfigFile()
            ->hasViews();
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'form-components');
        $this->registerLivewireComponents();

        $this->publishes([
            __DIR__.'/../resources/assets/flatpickr' => public_path('vendor/flatpickr'),
        ], 'public');
    }

    /**
     * Por compatibilidad con paquetes antiguos de bPanel4, registro los componentes como form::xxxx y form.xxxx
     */
    private function registerLivewireComponents(): void
    {
        foreach (['form::', 'form.'] as $prefix) {
            Livewire::component($prefix . 'actions', Actions::class);
            Livewire::component($prefix . 'dual-list-box', DualListBox::class);
            Livewire::component($prefix . 'input-checkbox', InputCheckbox::class);
            Livewire::component($prefix . 'input-date', InputDate::class);
            Livewire::component($prefix . 'input-file', InputFile::class);
            Livewire::component($prefix . 'input-hidden', InputHidden::class);
            Livewire::component($prefix . 'input-number', InputNumber::class);
            Livewire::component($prefix . 'input-password', InputPassword::class);
            Livewire::component($prefix . 'input-text', InputText::class);
            Livewire::component($prefix . 'input-text-language', InputTextLanguage::class);
            Livewire::component($prefix . 'input-email', InputEmail::class);
            Livewire::component($prefix . 'save-button', SaveButton::class);
            Livewire::component($prefix . 'select', Select::class);
            Livewire::component($prefix . 'textarea', Textarea::class);
            Livewire::component($prefix . 'radio', Radio::class);
            Livewire::component($prefix . 'input-color', InputColor::class);
        }
    }
}
