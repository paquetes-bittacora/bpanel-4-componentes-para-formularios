<div class="form-group form-row">
    <div class="col-sm-{{ $labelWidth }} col-form-label text-sm-right">
        <label class="mb-0  @error($name) text-danger-d1 @enderror">
            @if($required)
                <span class="text-danger">*</span>
            @endif {{ $labelText }}
        </label>
    </div>
    <div class="col-sm-{{ $fieldWidth }}">
        @foreach($options as $value => $label)
            <label>
                <input type="radio" name="{{ $name }}"
                       value="{{ $value }}"
                       id="@if(!is_null($idField)){{$idField}}@else{{$name}}@endif"
                       @if($required) required @endif
                       @if($disabled or ($readonly and $value !== $selectedOption))
                           disabled
                       @endif
                        @if($selectedOption === $value) checked @endif
                />
                {{ $label }}
            </label>
        @endforeach

        @error($name)
        <small class="clearfix text-danger">{{$message}}</small>
        @enderror
    </div>
</div>
