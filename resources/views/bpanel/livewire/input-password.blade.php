<div class="form-group form-row">
    <div class="col-sm-{{ $labelWidth }} col-form-label text-sm-right">
        <label for="id-form-field-1" class="mb-0  @error($name) text-danger-d1 @enderror">
            @if($required) <span class="text-danger">*</span> @endif {{ $labelText }}: &nbsp;
        </label>
    </div>

    <div class="col-sm-{{ $fieldWidth }}">
        @if(!is_null($icon))
            <div class="input-group m-b">
                @if($alignIcon == 'prepend')
                    <div class="input-group-prepend">
                        <span class="input-group-addon">{{ $icon }}</span>
                    </div>
                @endif
                @endif
                <input type="password" name="{{$name}}"
                       class="form-control @if(!is_null($cssClasses)) @foreach($cssClasses as $cssClass) {{$cssClass}} @endforeach @endif"
                       id="@if(!is_null($idField)){{$idField}}@else{{$name}}@endif"
                       @if(!is_null($placeholder)) placeholder="{{$placeholder}}" @endif
                       @if(!is_null(old($name)))
                       value="{{old($name)}}"
                       @else
                       @if(!is_null($value)) value="{{$value}}" @endif
                       @endif
                       @if(!is_null($minlength)) minlength="{{$minlength}}" @endif
                       @if($required) required @endif
                       @if(!is_null($pattern)) pattern="{{$pattern}}" @endif
                       @if($readonly) readonly @endif
                       @if(!is_null($size)) size="{{$size}}" @endif
                       @if(!is_null($autocomplete)) autocomplete="{{$autocomplete}}" @endif
                       @if($autofocus) autofocus @endif
                       @if($disabled) disabled @endif
                       @if(!is_null($max)) max="{{$max}}" @endif
                       @if(!is_null($min)) min="{{$min}}" @endif
                       @if(!is_null($spellcheck)) spellcheck="{{$spellcheck}}" @endif
                >

                @if(!is_null($icon))
                    @if($alignIcon == 'append')
                        <div class="input-group-append">
                            <span class="input-group-addon">{{ $icon }}</span>
                        </div>
                    @endif
            </div>
        @endif
                @if(!is_null($note))
                    <small class="d-block">{{$note}}</small>
                @endif

        @error($name)
        <small class="clearfix text-danger">{{$message}}</small>
        @enderror
    </div>
</div>
