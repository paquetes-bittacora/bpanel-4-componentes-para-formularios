@pushOnce('styles')
    @vite('node_modules/flatpickr/dist/flatpickr.css')
@endpushOnce

@pushOnce('scripts')
    @vite('node_modules/flatpickr/dist/flatpickr.js?commonjs-entry')
@endpushOnce

<div>
    <div class="form-group form-row">
        <div class="col-sm-{{$labelWidth}} col-form-label text-sm-right">
            <label for="id-form-field-1" class="mb-0  @error($name) text-danger-d1 @enderror">
                @if($required) <span class="text-danger">*</span> @endif {{ $labelText }}
            </label>
        </div>

        <div class="col-sm-{{$fieldWidth}}">
            <input type="text"  name="{{$name}}" @if(!is_null($idField)) id="{{$idField}}" @else id="{{$name}}" @endif class="form-control">
        </div>
    </div>
</div>

@push('scripts')


    <script>
      /**
       * Originalmente esto lo teníamos con jQuery, haciendo la llamada tipo $(...).flatpickr, pero no funcionaba
       * y lo he modificado un poco para que vuelva a funcionar.
       */
      window.addEventListener('load', function(event) {
        flatpickr(document.getElementById("@if(!is_null($idField)){{$idField}}@else{{$name}}@endif"), {
          enableTime: @if($enableTime) true @else false @endif,
          dateFormat: "{{$dateFormat}}",
          time_24hr: true,
          defaultDate: @if(!is_null($value))"{{$value}}"@else"{{$defaultDate}}"@endif,
          locale: {
            firstDayOfWeek: 1,
            weekdays: {
              shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
              longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            },
            months: {
              shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
              longhand: ['Enero', 'Febrero', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            },
          },
        });
      });
    </script>
@endpush
