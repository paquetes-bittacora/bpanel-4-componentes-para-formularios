<div class="form-group form-row">
    <div class="col-sm-{{ $labelWidth }} col-form-label text-sm-right">
        <label for="@if(!is_null($idField)){{$idField}}@else{{$name}}@endif" class="mb-0  @error($name) text-danger-d1 @enderror">
            @if($required) <span class="text-danger">*</span> @endif {{ $labelText }}
        </label>
    </div>

    <div class="col-sm-{{ $fieldWidth }}">
        @if(!is_null($icon) or !is_null($image))
            <div class="input-group m-b">
                @if($alignIcon == 'prepend')
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                            {@if(!is_null($icon))
                                {{ $icon }}
                            @else
                                <img src="{{$image}}" alt="">
                            @endif
                        </span>
                    </div>
                @endif
                @endif
                <textarea type="text" rows="{{$rows}}" name="{{$name}}"
                       class="form-control @if(!is_null($cssClasses)) @foreach($cssClasses as $cssClass) {{$cssClass}} @endforeach @endif"
                       id="@if(!is_null($idField)){{$idField}}@else{{$name}}@endif"
                       @if(!is_null($placeholder)) placeholder="{{$placeholder}}" @endif
                       @if($required) required @endif
                       @if($readonly) readonly @endif
                       @if(!is_null($autocomplete)) autocomplete="{{$autocomplete}}" @endif
                       @if($autofocus) autofocus @endif
                       @if($disabled) disabled @endif
                >@if(!is_null(old($name))){{old($name)}}@else{{""}}@if(!is_null($value)){{$value}}@endif{{""}}@endif</textarea>

                @if(!is_null($icon) or !is_null($image))
                    @if($alignIcon == 'append')
                        <div class="input-group-append">
                            <span class="input-group-text">
                                @if(!is_null($icon))
                                    {{ $icon }}
                                @else
                                    <img src="{{$image}}" alt="">
                                @endif
                            </span>
                        </div>
                    @endif
            </div>
        @endif

        @error($name)
        <small class="clearfix text-danger">{{$message}}</small>
        @enderror

        @if(!is_null($customErrorMessage))
            <small class="clearfix text-danger d-block">{{$customErrorMessage}}</small>
        @endif
    </div>
</div>
