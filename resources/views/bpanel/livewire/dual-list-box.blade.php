@if (!empty($labelText))
    <div class="card-header bgc-primary-d1 text-white border-0">
        <h4 class="text-120">
            <span class="text-90">{{ $labelText }}</span>
        </h4>
    </div>
@endif

<div class="bootstrap-duallistbox-container row moveonselect moveondoubleclick mb-4 pt-2 pb-2">
    <div class="box1 col-12">
        <select multiple="multiple" id="{{$idField}}" class="form-control" name="{{$name}}" style="height: 232px;" @if(empty($allValues)) disabled @endif>
            @forelse($allValues as $key => $value)
                <option value="{{$key}}" @if(in_array($key, $selectedValues)) selected @endif>{{ $value }}</option>
            @empty
                <option value="">-- No hay datos --</option>
            @endforelse
        </select>
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded' ,function(){
            $("#{{$idField}}").bootstrapDualListbox({
              filterTextClear: 'Mostrar todos',
              filterPlaceHolder: 'Filtrar',
              moveSelectedLabel: 'Mover seleccionados',
              moveAllLabel: 'Mover todos',
              removeSelectedLabel: 'Quitar seleccionados',
              removeAllLabel: 'Quitar todos',
              infoText: 'Mostrando todos ({0})',
              infoTextFiltered: '<span class="label label-warning">Filtrando</span> {0} de {1}',
              infoTextEmpty: "Lista vacía"
            });
            $('.moveall i').removeClass().addClass('fa fa-arrow-right');
            $('.move i').removeClass().addClass('fa fa-arrow-right');
            $('.removeall i').removeClass().addClass('fa fa-arrow-left');
            $('.remove i').removeClass().addClass('fa fa-arrow-left');
        });
    </script>
@endpush
