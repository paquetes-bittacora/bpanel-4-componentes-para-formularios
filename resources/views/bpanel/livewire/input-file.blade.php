<div class="form-group form-row">
    @if($fieldWidth < 12)
        <div class="col-sm-{{ $labelWidth }} col-form-label text-sm-right">
            <label for="id-form-field-1" class="mb-0  @error($name) text-danger-d1 @enderror">
                @if($required) <span class="text-danger">*</span> @endif {{ $labelText }}
            </label>
        </div>
    @endif

    <div class="col-sm-{{ $fieldWidth }}">
        <div class="file-loading">
            <input type="file" name="{{$name}}"
                   @if(!is_null($idField)) id="{{ \Illuminate\Support\Str::slug($idField) }}" @else id="{{$name}}" @endif
                   @if(!is_null($files)) files="{{$files}}" @endif
                   @if(!is_null($accept))accept="{{$accept}}" @endif
                   @if(!$capture) capture @endif
                   @if($multiple) multiple @endif
                   @if($hideInput) class="d-none" @endif
            >
        </div>
        @error($name)
        <small class="clearfix text-danger d-block">{{$message}}</small>
        @enderror
    </div>
</div>

@push('scripts')
    <script>
        @if($csrf)
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        @endif
        window.addEventListener('load', function(event) {
          var input;
            @if(!is_null($idField))
            input = jQuery("#{{\Illuminate\Support\Str::slug($idField)}}");
            @else
            input = jQuery("#{{$name}}");
            @endif

          var properties = {
            'dropZoneEnabled': {{ json_encode($dropZoneEnabled) }},
            'showUpload': {{ json_encode($showUpload) }},
            'showCaption': {{ json_encode($showCaption) }},
            'showRemove': {{ json_encode($showRemove) }},
            'theme': "{{ $theme }}",
            'language': "{{ $language }}",
            'minFileCount': {{ $minFileCount }},
            'maxFileCount': {{ $maxFileCount }},
            'browseOnZoneClick': {{ $browseOnZoneClick }},
            'msgUploadError': '{{ $msgUploadError }}',
            'overwriteInitial': false,
            'uploadAsync': {{ json_encode($uploadAsync) }},
            'initialPreviewAsData': {{ json_encode($initialPreviewAsData) }},
            'previewClass': "bg-light",
            'previewFileIconSettings': {
              'doc': '<i class="fa fa-file-word-o text-primary"></i>',
              'xls': '<i class="fa fa-file-excel-o text-success"></i>',
              'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
              'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
              'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
              'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
              'rar': '<i class="fa fa-file-archive-o text-muted"></i>',
              '7z': '<i class="fa fa-file-archive-o text-muted"></i>',
            }
          }

            @if(!is_null($allowedFileExtensions))
          var allowedFileExtensions = [];

          $.each({!! json_encode($allowedFileExtensions) !!}, function(key, value){
            allowedFileExtensions.push(value);
          });

          properties['allowedFileExtensions'] = allowedFileExtensions;
            @endif

                    @if(isset($uploadUrl) AND !is_null($uploadUrl))
            properties['uploadUrl'] = {!! json_encode($uploadUrl) !!}
                    @endif

                    @if(!is_null($deleteUrl))
            properties['deleteUrl'] = {!! json_encode($deleteUrl) !!}
            @endif

            input.fileinput(properties).on('fileuploaderror', function(event, data, msg){
              console.log(msg);
            }).on("filebatchselected", function(event, files) {
              input.fileinput("upload");
            }).on("filebatchuploadcomplete", function(msg){
              Swal.fire({
                icon: 'success',
                title: 'Confirmación',
                text: 'Los archivos se han almacenado correctamente.',
                showConfirmButton: false,
                timer: 2000
              });
              setTimeout(function(){
                input.fileinput("clear");
              }, 2000);
            });
        });
    </script>
@endpush
