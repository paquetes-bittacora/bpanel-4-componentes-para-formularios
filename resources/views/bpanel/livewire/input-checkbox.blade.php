@if($bpanelForm)
    <div class="form-group form-row">
        <div class="col-sm-{{ $labelWidth }} col-form-label text-sm-right">
        </div>
        <div class="col-sm-{{ $fieldWidth }} col-form-label">
            <div class="form-check form-check-inline text-center">
                <input class="form-check-input ace-switch"
                       type="checkbox"
                       @if($multiple) name="{{$name}}[]" @else name="{{$name}}" @endif
                       id="{{$idField}}"
                       @if (old($name) OR $checked) checked @endif
                       @if (!is_null($value)) value="{{$value}}" @endif
                       @if (isset($disabled) AND $disabled==true) disabled @endif
                >
                @if(!is_null($labelText))
                    <label class="form-check-label" for="{{$idField}}">{{ $labelText }}</label>
                @endif
            </div>
            @error($name)
            <div class="row">
            <small class="clearfix text-danger">{{$message}}</small>
            </div>
            @enderror
        </div>
    </div>
@else
    <div class="form-check form-check-inline text-center mr-0">
        <input class="form-check-input ace-switch mr-0"
               type="checkbox"
               @if($multiple) name="{{$name}}[]" @else name="{{$name}}" @endif
               id="{{$idField}}"
               @if($checked) checked @endif
                @if (!is_null($value)) value="{{$value}}" @endif
               @if (isset($disabled) AND $disabled==true) disabled @endif
        >
        @if(!is_null($labelText))
            <label class="form-check-label" for="{{$idField}}">{{ $labelText }}</label>
        @endif
        @error($name)
        <small class="clearfix text-danger">{{$message}}</small>
        @enderror
    </div>
@endif

