<div class="form-group form-row">
    <div class="col-sm-{{ $labelWidth }} col-form-label text-sm-right">
        <label for="id-form-field-1" class="mb-0  @error($name) text-danger-d1 @enderror">
            @if($required) <span class="text-danger">*</span> @endif {{ $labelText }}
        </label>
    </div>

    <div class="col-sm-{{ $fieldWidth }}">
        <div class="input-group m-b">
            <input type="color" name="{{$name}}"
                   class="form-control"
                   id="@if(!is_null($idField)){{$idField}}@else{{$name}}@endif"
                   @if(!is_null(old($name)))
                       value="{{old($name)}}"
                   @else
                       @if(!is_null($value)) value="{{$value}}" @endif
                   @endif
                   @if($required) required @endif
                   @if($disabled) disabled @endif
            >
        </div>
        @error($name)
        <small class="clearfix text-danger d-block">{{$message}}</small>
        @enderror

        @if(!is_null($customErrorMessage))
            <small class="clearfix text-danger d-block">{{$customErrorMessage}}</small>
        @endif
    </div>
</div>
