<div class="form-group form-row">
    <div class="col-sm-{{ $labelWidth }} col-form-label text-sm-right">
        <label for="id-form-field-1" class="mb-0  @error($name) text-danger-d1 @enderror">
            @if($required) <span class="text-danger">*</span> @endif {{ $labelText }}
        </label>
    </div>
    <div class="col-sm-{{ $fieldWidth }}">
        <select name="{{ $name }}"
                id="@if(!is_null($idField)){{$idField}}@else{{$name}}@endif"
                @if($required) required @endif
                @if($disabled) disabled @endif
                @if($multiple) multiple @endif
                @if($readonly) readonly @endif
                class="form-control select2">

            @if(!is_null($placeholder))
                <option></option>
            @endif
            @if(!empty($allValues))
                @foreach($allValues as $key => $value)
                    <option value="{{ $key }}" @if(isset($selectedValues) and in_array($key, $selectedValues)) selected @endif>{{ $value }}</option>
                @endforeach
            @endif
        </select>

        @error($name)
        <small class="clearfix text-danger">{{$message}}</small>
        @enderror
    </div>
</div>

@push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            @if(!is_null($placeholder))
                @if(!is_null($idField))
                    $("#{{$idField}}").select2({
                        placeholder: "{{$placeholder}}",
                    });
                @else
                    $("#{{$name}}").select2({
                        placeholder: "{{$placeholder}}",
                    });
                @endif
            @endif
        });
    </script>
@endpush
