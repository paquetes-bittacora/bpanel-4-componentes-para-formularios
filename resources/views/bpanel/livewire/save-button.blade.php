<div>
    <button class="btn btn-{{ $color }} btn-bold px-4 mx-2" type="{{$type}}" name="{{ $name }}" id="@if(!is_null($idField)){{$idField}}@else{{$name}}@endif">
        <i class="{{ $icon }} mr-1" aria-hidden="true"></i>
        &nbsp;{{ $text }}
    </button>
</div>
